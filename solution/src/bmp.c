#define _POSIX_C_SOURCE 200112L
#include "bmp.h"
#include "image.h"
#include "util.h"
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

struct stat statbuf;


struct bmp_header* ihdr_ptr;

void calc_padded_width(struct image* img){
	(*img).padded_width = (*img).width*3;
	if((*img).padded_width % 4){
		(*img).padded_width += ( 4 - (*img).padded_width % 4);
	}
}


struct image* i_bmp_toimg(int fd){
	fstat(fd, &statbuf);
	void* mmap_ptr = mmap(0, statbuf.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	
	struct bmp_header* hdr_ptr = mmap_ptr;

	struct image* iimg = (struct image*)malloc(sizeof(struct image));

	(*iimg).width = (*hdr_ptr).biWidth; 
	(*iimg).height = (*hdr_ptr).biHeight;
	calc_padded_width(iimg);
	(*iimg).pixels = (char*)mmap_ptr + (*hdr_ptr).bOffBits;

	ihdr_ptr = hdr_ptr;

	return iimg;
}

struct image* o_bmp_toimg(int fd, struct image* iimg){
	struct image* oimg = (struct image*)malloc(sizeof(struct image));

	(*oimg).height = (*iimg).width;
	(*oimg).width = (*iimg).height;
	calc_padded_width(oimg);

	ftruncate(fd, (*oimg).padded_width * (*oimg).height + (*ihdr_ptr).bOffBits);

	void* mmap_ptr = mmap(0, (*oimg).padded_width * (*oimg).height + sizeof(struct bmp_header), PROT_WRITE, MAP_SHARED, fd, 0);

	struct bmp_header* hdr_ptr = mmap_ptr;

	my_memcpy(hdr_ptr, ihdr_ptr, (*ihdr_ptr).bOffBits);
	(*hdr_ptr).biWidth = (*oimg).width;
	(*hdr_ptr).biHeight = (*oimg).height;
	(*hdr_ptr).bfileSize = (uint32_t)((*oimg).padded_width * (*oimg).height + (*ihdr_ptr).bOffBits);
	(*hdr_ptr).biSizeImage = (uint32_t)((*oimg).padded_width * (*oimg).height);
	(*hdr_ptr).biXPelsPerMeter = 0;
	(*hdr_ptr).biYPelsPerMeter = 0;

	//prntbmphdr(ihdr_ptr, hdr_ptr);	

	(*oimg).pixels = (char*)mmap_ptr + (*hdr_ptr).bOffBits;

	return oimg;
}
