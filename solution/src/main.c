#include "image.h"
#include "bmp.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>


int main(int argc, char** argv){

	if(argc != 3){
		return 1;
	}

	int fdi = open(argv[1], O_RDWR);
	int fdo = open(argv[2], 0x0042, 0x1B6);

	struct image* imgi = i_bmp_toimg(fdi);
	printf("%dx%d\n", (*imgi).width, (*imgi).height);
	struct image* imgo = o_bmp_toimg(fdo, imgi);
	printf("%dx%d\n", (*imgo).width, (*imgo).height);
	turn_img(imgi, imgo);
	free(imgi);
	free(imgo);
}
