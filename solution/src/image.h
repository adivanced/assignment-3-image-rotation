#ifndef __IMAGE_H__
#define __IMAGE_H__


#include <stdint.h>

struct image{
	uint32_t width;
	uint32_t height;
	uint32_t padded_width;

	void* pixels;
};

struct pixel{
	uint8_t R;
	uint8_t G;
	uint8_t B;
};

void turn_img(struct image* imgi, struct image* imgo);

void calc_padded_width(struct image* img);

#endif
