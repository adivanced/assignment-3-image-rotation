#include <stdint.h>

void my_memcpy(void* dest, void* src, uint64_t sz){
	__asm__ ("cld\t\n"
		"movq %%rdx, %%rcx \t\n"
		"rep movsb\t\n"
		:
    	: "D" (dest), "S" (src), "d" (sz)
        : "rcx", "cc"
	);
}


