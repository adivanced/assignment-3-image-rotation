#define __STDC_WANT_LIB_EXT1__ 1

#include "image.h"
#include "util.h"
#include <string.h>





void turn_img(struct image* imgi, struct image* imgo){
	void* src = (*imgi).pixels;
	void* dest = (*imgo).pixels;
	uint32_t cntx;
	uint32_t cnty;
	for(cnty = 0; cnty < imgi->height; cnty++){
		for(cntx = 0; cntx < imgi->width; cntx++){
			my_memcpy(((char*)dest + (cntx)*(*imgo).padded_width + ((*imgo).width - 1 -cnty)*3), src, 3);
			src = (char*)src + 3;
		}
		src = ((char*)src + ((*imgi).padded_width - (*imgi).width*3));
	}
}
